General
	Part Number	ISIS_iSPA_2U_BMSP.stp
	Part Name	ISIS_iSPA_2U_BMSP.stp
	Description
	Material Name	Steel

Manage
	Item Number
	Lifecycle
	Revision
	State	Working
	Change Order

Physical
	Mass	238.488 g
	Volume	30380.637 mm^3
	Density	0.008 g / mm^3
	Area	36346.664 mm^2
	World X,Y,Z	0.00 mm, 101.50 mm, 155.75 mm
	Center of Mass	0.085 mm, 100.856 mm, 156.077 mm
	Bounding Box
		Length	82.60 mm
		Width	3.87 mm
		Height	211.50 mm
	Moment of Inertia at Center of Mass   (g mm^2)
		Ixx	8.804E+05
		Ixy	25.432
		Ixz	-1129.026
		Iyx	25.432
		Iyy	1.013E+06
		Iyz	263.002
		Izx	-1129.026
		Izy	263.002
		Izz	1.323E+05
	Moment of Inertia at Origin   (g mm^2)
		Ixx	9.116E+06
		Ixy	-2029.53
		Ixz	-4309.125
		Iyx	-2029.53
		Iyy	6.822E+06
		Iyz	-3.754E+06
		Izx	-4309.125
		Izy	-3.754E+06
		Izz	2.558E+06

