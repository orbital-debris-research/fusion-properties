General
	Part Number	camera_mount
	Part Name	camera_mount
	Description
	Material Name	SOLIDWORKS Materials|ABS PC

Manage
	Item Number
	Lifecycle
	Revision
	State	Working
	Change Order

Physical
	Mass	1.066 g
	Volume	995.90 mm^3
	Density	0.001 g / mm^3
	Area	2069.301 mm^2
	World X,Y,Z	-15.107 mm, 80.089 mm, 0.00 mm
	Center of Mass	-26.077 mm, 77.19 mm, -3.791E-06 mm
	Bounding Box
		Length	15.00 mm
		Width	29.904 mm
		Height	16.00 mm
	Moment of Inertia at Center of Mass   (g mm^2)
		Ixx	106.388
		Ixy	-14.146
		Ixz	4.366E-05
		Iyx	-14.146
		Iyy	50.609
		Iyz	2.246E-05
		Izx	4.366E-05
		Izy	2.246E-05
		Izz	96.709
	Moment of Inertia at Origin   (g mm^2)
		Ixx	6455.675
		Ixy	2130.848
		Ixz	-6.169E-05
		Iyx	2130.848
		Iyy	775.257
		Iyz	0.00
		Izx	-6.169E-05
		Izy	0.00
		Izz	7170.644

