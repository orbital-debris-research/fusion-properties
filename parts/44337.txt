General
	Part Number	Runcam Racer Nano.step
	Part Name	Runcam Racer Nano.step
	Description
	Material Name	Steel

Manage
	Item Number
	Lifecycle
	Revision
	State	Working
	Change Order

Physical
	Mass	12.203 g
	Volume	1554.512 mm^3
	Density	0.008 g / mm^3
	Area	2351.908 mm^2
	World X,Y,Z	-22.107 mm, 87.389 mm, 0.00 mm
	Center of Mass	-22.19 mm, 89.222 mm, -0.048 mm
	Bounding Box
		Length	14.00 mm
		Width	14.90 mm
		Height	14.078 mm
	Moment of Inertia at Center of Mass   (g mm^2)
		Ixx	423.832
		Ixy	-5.80
		Ixz	3.527
		Iyx	-5.80
		Iyy	347.356
		Iyz	-3.41
		Izx	3.527
		Izy	-3.41
		Izz	419.172
	Moment of Inertia at Origin   (g mm^2)
		Ixx	97565.574
		Ixy	24153.535
		Ixz	-9.421
		Iyx	24153.535
		Iyy	6355.859
		Iyz	48.655
		Izx	-9.421
		Izy	48.655
		Izz	1.036E+05

