General
	Part Number	projectile
	Part Name	projectile
	Description
	Material Name	SOLIDWORKS Materials|Plain Carbon Steel

Manage
	Item Number
	Lifecycle
	Revision
	State	Working
	Change Order

Physical
	Mass	0.847 g
	Volume	108.651 mm^3
	Density	0.008 g / mm^3
	Area	274.826 mm^2
	World X,Y,Z	-12.50 mm, 60.07 mm, -12.50 mm
	Center of Mass	-12.192 mm, 62.501 mm, -12.192 mm
	Bounding Box
		Length	10.037 mm
		Width	10.736 mm
		Height	10.037 mm
	Moment of Inertia at Center of Mass   (g mm^2)
		Ixx	6.898
		Ixy	0.237
		Ixz	-0.964
		Iyx	0.237
		Iyy	10.173
		Iyz	0.237
		Izx	-0.964
		Izy	0.237
		Izz	6.898
	Moment of Inertia at Origin   (g mm^2)
		Ixx	3443.451
		Ixy	646.026
		Ixz	-126.936
		Iyx	646.026
		Iyy	262.119
		Iyz	646.026
		Izx	-126.936
		Izy	646.026
		Izz	3443.451

