General
	Part Number	Sun Sensor
	Part Name	Sun Sensor
	Description
	Material Name	Steel

Manage
	Item Number
	Lifecycle
	Revision
	State	Working
	Change Order

Physical
	Mass	11.338 g
	Volume	1444.311 mm^3
	Density	0.008 g / mm^3
	Area	1210.691 mm^2
	World X,Y,Z	50.00 mm, -6.50 mm, -53.00 mm
	Center of Mass	50.00 mm, -6.838 mm, -53.505 mm
	Bounding Box
		Length	11.00 mm
		Width	33.50 mm
		Height	6.00 mm
	Moment of Inertia at Center of Mass   (g mm^2)
		Ixx	1185.612
		Ixy	0.00
		Ixz	0.00
		Iyx	0.00
		Iyy	137.268
		Iyz	22.007
		Izx	0.00
		Izy	22.007
		Izz	1263.292
	Moment of Inertia at Origin   (g mm^2)
		Ixx	34173.049
		Ixy	3876.27
		Ixz	30331.342
		Iyx	3876.27
		Iyy	60939.207
		Iyz	-4125.96
		Izx	30331.342
		Izy	-4125.96
		Izz	30137.994

