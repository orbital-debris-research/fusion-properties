#Persistent
OnClipboardChange("ClipChanged")
return

ClipChanged(Type) {
;    _____ _                            
;   / ____| |                           
;  | |    | |__   __ _ _ __   __ _  ___ 
;  | |    | '_ \ / _` | '_ \ / _` |/ _ \
;  | |____| | | | (_| | | | | (_| |  __/
;   \_____|_| |_|\__,_|_|_|_|\__, |\___|
;            |  \/  |  ____|  __/ |     
;            | \  / | |__    |___/      
;            | |\/| |  __|              
;            | |  | | |____             
;            |_|  |_|______|       
;           
;             \/\/\/\/\/\/\/\/\/\/\/\/\/\/
outputDir := "C:\Coding\fusion-properties\parts\"


Random, rand, 10000, 99999

file := FileOpen(outputDir . rand . ".txt","w")

file.Write(clipboard)
file.Close()
}

Esc::ExitApp  ; Exit script with Escape key
